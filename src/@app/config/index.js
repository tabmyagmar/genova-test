/* eslint-disable operator-linebreak */
// DON'T EDIT THIS FILE
// PUT YOUR CONFIGURATION VALUES IN the .env FILE.
// THERE ARE SAMPLE VALUES IN .env.example

export default {
  BACKEND_URL: process.env.BACKEND_URL || 'http://localhost:4000/api/graphql',
  TOKEN_KEY: process.env.TOKEN_KEY || 'token',
};
