import React from 'react';
import { Form, Input, Button } from 'antd';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { LOGIN } from '@app/scenes/utils/gql';
import { useUserContext } from '@app/userProvider';
import Cookies from 'js-cookie';
import config from '@app/config';
import { get } from 'lodash';

const LoginScene = () => {
  const apolloClient = useApolloClient();
  const { user } = useUserContext();

  const [login] = useMutation(LOGIN, {
    onCompleted: ({ login: data }) => {
      const { token } = data;
      const token_key = config.TOKEN_KEY;
      Cookies.remove(token_key);

      document.cookie = Cookies.set(token_key, token, {
        expires: 1,
        htmlType: true,
        secure: false,
        path: '/',
      });

      apolloClient.cache.reset().then(() => {
        window.location.reload();
      });
    },
    onError: (error) => console.log('login error', error),
  });

  const onFinish = (value) => {
    login({
      variables: { data: value },
    });
  };

  const logout = () => {
    Cookies.remove(config.TOKEN_KEY);
    apolloClient.cache.reset().then(() => {
      setTimeout(() => window.location.reload(), 500);
    });
  };

  return (
    <div>
      {get(user, 'adminEmail') ? (
        <div>
          adminEmail: {get(user, 'adminEmail')}
          <button onClick={logout}>Logout</button>
        </div>
      ) : (
        <Form
          name='login'
          initialValues={{ adminEmail: '', password: '' }}
          onFinish={onFinish}
        >
          <Form.Item name='adminEmail'>
            <Input></Input>
          </Form.Item>
          <Form.Item name='password'>
            <Input></Input>
          </Form.Item>
          <Button htmlType='submit'>Login</Button>
        </Form>
      )}
    </div>
  );
};

export default LoginScene;
