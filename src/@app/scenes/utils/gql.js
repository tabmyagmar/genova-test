import { gql } from '@apollo/client';

export const LOGIN = gql`
  mutation LOGIN($data: LoginInput!) {
    login(data: $data) {
      token
    }
  }
`;

export const ME = gql`
  query ME {
    me {
      id
      name
      nameKana
      adminFirstName
      adminLastName
      adminEmail
      passwordSet
      showSettingForm
      reception
      receptionCount
      webPageUrl
      hospitalSetting {
        id
        firstAppointment
        notification
        notificationSound
      }
      hospitalDepartments {
        id
        name
      }
    }
  }
`;
