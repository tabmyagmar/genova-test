import Router from 'next/router';
import cookie from 'cookie';
import Cookies from 'js-cookie';
import get from 'lodash/get';
import { ME } from '@app/scenes/utils/gql';
import config from '@app/config';

const redirect = (res, route) => {
  if (process.browser) {
    Router.push(route);
  } else {
    res.writeHead(303, { Location: route });
    res.end();
  }
};

const getUser = async (apolloClient) => {
  const response = await apolloClient.query({
    query: ME,
  });

  return get(response, 'data.me');
};

const parseCookies = ({ req }) => {
  return cookie.parse(req ? req.headers.cookie || '' : document.cookie || '');
};

const logout = async (res, apolloClient, token, route) => {
  if (process.browser) {
    Cookies.remove(token);
    document.cookie = cookie.serialize(token, '', {
      maxAge: -1,
      path: '/',
    });
  } else {
    res.setHeader(
      'Set-Cookie',
      cookie.serialize(token, '', {
        maxAge: -1,
        path: '/',
      }),
    );
  }

  apolloClient.cache
    .reset()
    .then(() => {
      console.log('Logout apolloClient reset: ', route, '\n');
      redirect(res, route);
    })
    .catch((err) => console.log(err));
};

const auth = async ({ req, res, apolloClient }) => {
  const cookies = parseCookies({ req });
  const token_key = config.TOKEN_KEY;

  const TOKEN = process.browser ? Cookies.get(token_key) : cookies[token_key];

  let user = {};

  const logoutError = (hospital_id = '') => {
    if (hospital_id) {
      logout(res, apolloClient, token_key, '/');
    }

    logout(res, apolloClient, token_key, '/');
  };

  if (TOKEN) {
    try {
      user = await getUser(apolloClient);
    } catch (error) {
      logoutError(hospital_id);
      return {};
    }
  }

  console.log('user', user)
  return { user };
};

export default auth;
