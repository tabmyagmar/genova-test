import React from 'react';
import App from 'next/app';
import withApollo from 'next-with-apollo';
import { ApolloClient, InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import auth from '@app/middleware/auth';
import cookie from 'cookie';
import config from '@app/config';

import { UserProvider } from '@app/userProvider';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    const { req, res, pathname, apolloClient } = ctx;

    // headers
    if (!process.browser) {
      res.setHeader('X-XSS-Protection', '1; mode=block');
      res.setHeader('X-Frame-Options', 'SAMEORIGIN');
      res.setHeader('strict-transport-security', 'max-age=31536000');
    }

    try {
      const { user } = await auth({
        ctx,
        route: pathname,
        req,
        res,
        apolloClient,
      });
      return { pageProps, user };
    } catch (error) {
      console.log('_app error', error);
      return { pageProps };
    }
  }

  render() {
    const { Component, apollo, pageProps, user = {} } = this.props;

    return (
      <UserProvider user={user}>
        <ApolloProvider client={apollo}>
          <Component {...pageProps} />
        </ApolloProvider>
      </UserProvider>
    );
  }
}

export default withApollo(
  ({ initialState, headers: serverHeaders, cookies: serverCookies }) => {
    const httpLink = createHttpLink({
      uri: config.BACKEND_URL,
      credentials: 'include',
    });

    const token = process.browser
      ? cookie.parse(document.cookie || '')[config.TOKEN_KEY]
      : cookie.parse(serverHeaders.cookie || '')[config.TOKEN_KEY];

    const authLink = setContext((_, { headers }) => ({
      headers: {
        ...headers,
        Authorization: token ? `Bearer ${token}` : '',
      },
    }));

    const link = authLink.concat(httpLink);

    return new ApolloClient({
      link,
      ssrMode: true,
      cache: new InMemoryCache({
        // addTypename: false,
      }).restore(initialState || {}),
    });
  },
)(MyApp);
