import React from 'react';
import LoginScene from '@app/scenes/login.scene';

const Home = () => {
  return <LoginScene />;
};

export default Home;
