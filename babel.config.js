module.exports = {
  presets: [
    [
      'next/babel',
      {
        'preset-env': {
          useBuiltIns: 'usage',
          corejs: 3,
          targets: {
            ie: 11,
          },
        },
      },
    ],
  ],
  env: {
    production: {
      plugins: [],
    },
  },
  plugins: [
    [
      'import',
      {
        libraryName: 'antd',
        style: true,
      },
    ],
    [
      'module-resolver',
      {
        root: ['./src'],
      },
    ],
  ],
};
