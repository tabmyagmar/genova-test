/* eslint-disable no-param-reassign */
/* eslint-disable no-nested-ternary */
const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withLess = require('@zeit/next-less');
const lessToJS = require('less-vars-to-js');
const Dotenv = require('dotenv-webpack');
const fs = require('fs');
const path = require('path');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');

const themeVariables = lessToJS(
  fs.readFileSync(
    path.resolve(__dirname, './public/assets/less/antstyle.less'),
    'utf8',
  ),
);

if (typeof require !== 'undefined') {
  require.extensions['.less'] = () => {};
}

module.exports = withCSS(
  withLess(
    withSass({
      lessLoaderOptions: {
        modifyVars: themeVariables,
        javascriptEnabled: true,
      },
      webpack: (config, { isServer }) => {
        if (isServer) {
          const antStyles = /antd\/.*?\/style.*?/;
          const origExternals = [...config.externals];
          config.externals = [
            (context, request, callback) => {
              if (request.match(antStyles)) return callback();
              if (typeof origExternals[0] === 'function') {
                origExternals[0](context, request, callback);
              } else {
                callback();
              }
            },
            ...(typeof origExternals[0] === 'function' ? [] : origExternals),
          ];
          config.module.rules.unshift({
            test: antStyles,
            use: 'null-loader',
          });
        }
        config.module.rules.push({
          test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 100000,
            },
          },
        });
        config.plugins.push(new Dotenv({}));
        config.plugins.push(
          new FilterWarningsPlugin({
            exclude: /mini-css-extract-plugin[^]*Conflicting order between:/,
          }),
        );
        return config;
      },
      poweredByHeader: false,
    }),
  ),
);
